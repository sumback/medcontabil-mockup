<header id="header" class="header">
    <div class="header-menu">
        <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left btn-cor-accent-secundaria"><i class="fas fa-bars"></i></a>
            <div class="header-left d-inline-block">
                <h5 class="text-dark-blue">Olá João Carlos</h5>
            </div>
        </div>
        <div class="col-sm-5" id="btn-superior-direito">
            <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="configuracao-menu fas fa-cogs"></i>
                    <!-- <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar"> -->
                </a>
                <div class="user-menu dropdown-menu">
                    <a class="nav-link gif-loading" href="meu-perfil.php"><i class="fa fa-user"></i> Meu Perfil</a>
                    <a class="nav-link gif-loading" href="index.php"><i class="fa fa-power-off"></i> Sair</a>
                </div>
            </div>
        </div>
    </div>
</header>
