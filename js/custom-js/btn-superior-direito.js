var painelDireito = document.getElementById('header');
var btnOpcaoPerfil = document.getElementById('btn-opcao-perfil');
var btnOpcaoSair = document.getElementById('btn-opcao-sair');

function myFunction(x) {
    if (x.matches){
        painelDireito.removeAttribute('hidden');
        btnOpcaoPerfil.setAttribute('hidden','true');
        btnOpcaoSair.setAttribute('hidden','true');
    }else{
        painelDireito.setAttribute('hidden', 'true');
        btnOpcaoPerfil.removeAttribute('hidden');
        btnOpcaoSair.removeAttribute('hidden');
    }
}

var x = window.matchMedia("(min-width: 700px)")
myFunction(x) // Call listener function at run time
x.addListener(myFunction) // Attach listener function on state changes
