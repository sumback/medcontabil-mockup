$('.btn-timeline').click(function(){

    $('.btn-timeline').each(function(){
        var idBtnTimeline = $(this).attr("id");
        var idDivConteudo = "d" + idBtnTimeline;
        $('#' + idDivConteudo).attr('hidden', true);
    });

    var idBtnTimeline = $(this).attr("id");
    var idDivConteudo = "d" + idBtnTimeline;

    if ($(this).hasClass("btn-timeline-focus")){
        $('#' + idDivConteudo).attr('hidden', true);
        $(this).removeClass('btn-timeline-focus');
    }else{
        $(this).addClass('btn-timeline-focus');
        $('#' + idDivConteudo).removeAttr('hidden');
        $('.btn-timeline').not(this).removeClass('btn-timeline-focus');
        var myScrollPos = $('.btn-timeline-focus').offset().left + $('.btn-timeline-focus').outerWidth(true)/2 + $('#timeline').scrollLeft() - $('#timeline').width()/2;
        $('#timeline').scrollLeft(myScrollPos);

    }
});
