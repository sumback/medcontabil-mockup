$(document).ready( function () {
    $('#tabela-padrao').DataTable( {
        language: {
            search:         "Pesquisar",
            lengthMenu:     "Mostrar _MENU_ Empresas",
            infoPostFix:    "",
            info:           "Mostrando _START_ de _END_ do total de _TOTAL_ empresas",
            loadingRecords: "Carregando...",
            zeroRecords:    "Nenhuma empresa encontrada",
            emptyTable:     "Nenhuma empresa cadastrada",
            paginate: {
                first:      "Primeiro",
                previous:   "Anterior",
                next:       "Próximo",
                last:       "Último"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    $('#tabela-etapas').DataTable( {
        language: {
            search:         "Pesquisar",
            lengthMenu:     "Mostrar _MENU_ Etapas",
            infoPostFix:    "",
            info:           "Mostrando _START_ de _END_ do total de _TOTAL_ etapas",
            loadingRecords: "Carregando...",
            zeroRecords:    "Nenhuma etapa encontrada",
            emptyTable:     "Nenhuma etapa cadastrada",
            paginate: {
                first:      "Primeiro",
                previous:   "Anterior",
                next:       "Próximo",
                last:       "Último"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
} );
