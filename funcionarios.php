<!doctype html>
<html class="no-js" lang="pt-br">

<?php include __DIR__ . '/head.php'; ?>

<body>
    <!-- Left Panel -->

    <?php include __DIR__ . '/menu-left.php'; ?>

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <?php include __DIR__ . '/menu-right-header.php'; ?>  

        <div id="carregando" class="center display-none">
            <div class="loading">
            </div>
        </div>

        <div class="container-fluid">
            <div class="content" id="conteudo">
                <div class="row mt-3">
                    <div class="col-md-9 mb-3">
                        <div class="row">
                            <div class="col text-dark-blue text-center text-md-left">
                                <h3>Funcionários</h3>
                            </div>
                        </div>
                        <hr class="bg-dark">
                    </div>
                    <div class="col-md-3">
                        <div class="card bg-light mb-3 text-center rounded borda-cor-primaria">
                            <div class="card-header bg-cor-primaria p-2"><b>PREVISÃO</b></div>
                            <div class="card-body texto-padrao p-2">
                                <h3 class="card-title"><i class="fas fa-stopwatch"></i></h3>
                                <h4 class="card-text"><b>5 Dias</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col text-center">
                        <button type="button" class="btn btn-primary btn-padrao btn-cor-primaria" data-toggle="modal" data-target=".bd-example-modal-lg">
                            <b>Nova Solicitação</b>
                        </button>
                    </div>
                </div>

                <!-- ********************* CARDS DE SOLICITAÇÕES ****************-->

                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="card texto-padrao bg-light mb-3 rounded border-warning">
                            <div class="card-header bg-warning text-white"><b>Solicitação Ativa</b></div>
                            <div class="card-body">
                                <h5 class="card-title">Situação: Em Andamento</h5>
                                <h6 class="card-title mt-4">Criação: 09/01/2019 11:27</h6>
                                <h6 class="card-title">Conclusão: 10/01/2019 12:00</h6>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header bg-cor-primaria">
                                <h5 class="modal-title" id="exampleModalLabel">Nova Solicitação</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-4 mb-3 text-center">
                                        <button type="button" class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block" data-toggle="modal" data-dismiss="modal" data-target="#modalAdmissao">Admissão</button>
                                    </div>
                                    <div class="col-md-4 mb-3 text-center">
                                        <button type="button" class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block" data-toggle="modal" data-dismiss="modal" data-target="#modalDemissao">Demissão</button>
                                    </div>
                                    <div class="col-md-4 mb-3 text-center">
                                        <button type="button" class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block" data-toggle="modal" data-dismiss="modal" data-target="#modalFerias">Férias</button>
                                    </div>
                                </div>
                                <div class="row justify-content-around">
                                    <div class="col-md-4 mb-3 text-center">
                                        <button type="button" class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block" data-toggle="modal" data-dismiss="modal" data-target="#modalOutro">Outros</button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary btn-padrao" data-dismiss="modal">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include __DIR__ . '/modal/funcionarios/admissao.php'; ?>
    <?php include __DIR__ . '/modal/funcionarios/demissao.php'; ?>
    <?php include __DIR__ . '/modal/funcionarios/ferias.php'; ?>
    <?php include __DIR__ . '/modal/funcionarios/relatorio-ferias.php'; ?>
    <?php include __DIR__ . '/modal/funcionarios/nova-ferias.php'; ?>
    <?php include __DIR__ . '/modal/funcionarios/outro.php'; ?>
    <?php include __DIR__ . '/footer.php'; ?>

</body>
</html>

<script>

    var funcionarios = [
        "Carlos Eduardo",
        "Luiz Carlos",
        "Maria Luiza",
    ];

    autocomplete(document.getElementById("input-demissao-nome"), funcionarios);
    autocomplete(document.getElementById("input-consulta-ferias-nome"), funcionarios);
    autocomplete(document.getElementById("input-emitir-ferias-nome"), funcionarios);

    $("input[type=file]").change(function () {
        var fieldVal = $(this).val();
        if (fieldVal != undefined || fieldVal != "") {
            $(this).next(".custom-file-label").text(fieldVal);
        }
    });
</script>