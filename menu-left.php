<aside id="left-panel" class="left-panel bg-cor-accent-primaria">
    <nav class="navbar navbar-expand-sm navbar-default bg-cor-accent-primaria" id="nav-panel">
        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars cor-accent-primaria"></i>
            </button>
            <a class="navbar-brand gif-loading border-white" href="/home">
                <img class="logo-marca" src="img/logo_marca2.png" alt="Logo">
            </a>
            <a class="navbar-brand hidden gif-loading" href="">
                <img src="img/favicon.png" alt="Logo">
            </a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="meu-perfil.php" class="gif-loading" id="btn-opcao-perfil"><i class="menu-icon fa fa-user"></i> Meu Perfil</a>
                    <a href="index.php" class="gif-loading"><i class="menu-icon fa fa-home"></i>Home</a>
                    <a href="emissao-nfs.php" class="gif-loading"><i class="menu-icon fas fa-user-alt"></i>Emissão de N.F.S.</a>
                    <a href="envio-documentos.php" class="gif-loading"><i class="menu-icon fas fa-file-upload"></i>Envio de Documentos</a>
                </li>
                <h3 class="menu-title border-white">Requisições de Serviços</h3>
                <li>
                    <a href="minhas-os.php" class="gif-loading"><i class="menu-icon fas fa-user-alt"></i>Minhas O.S.</a>
                    <a href="credenciamento.php" class="gif-loading"><i class="menu-icon fas fa-notes-medical"></i>Credenciamento </a>
                    <a href="faturamento.php" class="gif-loading"><i class="menu-icon fas fa-hand-holding-usd"></i>Faturamento </a>
                    <a href="emissao-certidao.php" class="gif-loading"><i class="menu-icon fas fa-calendar-check"></i>Emissão de Certidão </a>
                    <a href="recalculo-guias.php" class="gif-loading"><i class="menu-icon fa fa-calculator"></i>Recálculo de Guias </a>
                    <a href="funcionarios.php" class="gif-loading"><i class="menu-icon fas fa-user-alt"></i>Funcionários </a>
                    <a href="alteracao-contratual.php" class="gif-loading"><i class="menu-icon fas fa-file-signature"></i>Alteração Contratual </a>
                    <a href="declaracao-rendimento.php" class="gif-loading"><i class="menu-icon fas fa-hand-holding-usd"></i>Declaração Rendimento </a>
                    <a href="outros.php" class="gif-loading"><i class="menu-icon fa fa-comments"></i>Outros </a>
                    <a href="login.php" class="gif-loading" id="btn-opcao-sair"><i class="menu-icon fa fa-power-off"></i> Sair</a>
                </li>
            </ul>
        </div>
    </nav>
</aside>
