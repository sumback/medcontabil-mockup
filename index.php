<!doctype html>
<html class="no-js" lang="pt-br">

<?php include __DIR__ . '/head.php'; ?>

    <body>
        <!-- Left Panel -->

        <?php include __DIR__ . '/menu-left.php'; ?>

        <!-- Right Panel -->

        <div id="right-panel" class="right-panel">

            <?php include __DIR__ . '/menu-right-header.php'; ?>

            <div id="carregando" class="center display-none">
                <div class="loading">
                </div>
            </div>

            <div class="container-fluid">
                <div class="content mt-3" id="conteudo">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="social-box rounded-bottom">
                                <h5 class="titulo-widget bg-cor-accent-primaria pt-2 rounded-top">Total</h5>
                                <i class="fas fa-tasks bg-cor-accent-primaria m-0 social-box-line-heigth"></i>
                                <h5 class="bg-cor-accent-primaria pb-2">Ordem de Serviços</h5>
                                <ul>
                                    <li>
                                        <strong class="font-weight-bold">Pendentes</strong>
                                        <span>346</span>
                                    </li>
                                    <li>
                                        <strong class="font-weight-bold">Finalizadas</strong>
                                        <span>126</span>
                                    </li>
                                </ul>
                                <div class="row mt-3">
                                    <div class="col pr-md-0 text-center">
                                        <p>
                                            <button class="btn btn-padrao btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                                <strong>Ver Ativas</strong>
                                            </button>
                                        </p>
                                    </div>
                                </div>

                                <div class="collapse" id="collapseExample">
                                    <div class="card card-body border-0 p-1">
                                        <a href="credenciamento.php" class="text-dark p-2">Credenciamento <span class="badge bg-primary text-white">1</span></a>
                                        <a href="faturamento.php" class="text-dark p-2">Faturamento <span class="badge bg-info text-white">2</span></a>
                                        <a href="emissao-certidao.php" class="text-dark p-2">Emissão de Certidão <span class="badge bg-success text-white">4</span></a>
                                        <a href="recalculo-guias.php" class="text-dark p-2">Recálculo de Guias <span class="badge bg-danger text-white">8</span></a>
                                        <a href="funcionarios.php" class="text-dark p-2">Funcionários <span class="badge bg-warning text-dark">16</span></a>
                                        <a href="alteracao-contratual.php" class="text-dark p-2">Alteração Contratual <span class="badge bg-primary text-white">32</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <h5 class="py-3 bg-cor-accent-primaria rounded-top">Últimos Faturamentos</h5>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">Mês</th>
                                        <th scope="col">Valor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Janeiro</td>
                                        <td>R$ 10.000,00</td>
                                    </tr>
                                    <tr>
                                        <td>Fevereiro</td>
                                        <td>R$ 20.000,00</td>
                                    </tr>
                                    <tr>
                                        <td>Março</td>
                                        <td>R$ 30.000,00</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6 text-center">
                            <h5 class="py-3 bg-cor-accent-primaria rounded-top">Últimas Nfs Emitidas</h5>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">Tomador</th>
                                        <th scope="col">Serviço</th>
                                        <th scope="col">Data Emissão</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Mark</td>
                                        <td>Consulta</td>
                                        <td>01/01/2019</td>
                                    </tr>
                                    <tr>
                                        <td>Jacob</td>
                                        <td>Consulta</td>
                                        <td>01/01/2019</td>
                                    </tr>
                                    <tr>
                                        <td>Larry</td>
                                        <td>Consulta</td>
                                        <td>01/01/2019</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Right Panel -->

        <?php include __DIR__ . '/footer.php'; ?>

    </body>
</html>
