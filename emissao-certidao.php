<!doctype html>
<html class="no-js" lang="pt-br">

<?php include __DIR__ . '/head.php'; ?>

<body>
    <!-- Left Panel -->

    <?php include __DIR__ . '/menu-left.php'; ?>

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <?php include __DIR__ . '/menu-right-header.php'; ?>  

        <div id="carregando" class="center display-none">
            <div class="loading">
            </div>
        </div>

        <div class="container-fluid">
            <div class="content" id="conteudo">
                <div class="row mt-3">
                    <div class="col-md-9 mb-3">
                        <div class="row">
                            <div class="col text-dark-blue text-center text-md-left">
                                <h3>Emissão de Certidão</h3>
                            </div>
                        </div>
                        <hr class="bg-dark">
                    </div>
                    <div class="col-md-3">
                        <div class="card bg-light mb-3 text-center rounded borda-cor-primaria">
                            <div class="card-header bg-cor-primaria p-2"><b>PREVISÃO</b></div>
                            <div class="card-body texto-padrao p-2">
                                <h3 class="card-title"><i class="fas fa-stopwatch"></i></h3>
                                <h4 class="card-text"><b>4 Dias</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col text-center">
                        <button type="button" class="btn btn-primary btn-padrao btn-cor-primaria" data-toggle="modal" data-target=".bd-example-modal-lg">
                            <b>Nova Solicitação</b>
                        </button>
                    </div>
                </div>

                <!-- ********************* CARDS DE SOLICITAÇÕES ****************-->

                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="card texto-padrao bg-light mb-3 rounded border-success">
                            <div class="card-header bg-success text-white"><b>Solicitação Finalizada</b></div>
                            <div class="card-body">
                                <h5 class="card-title">Situação: Concluída</h5>
                                <h6 class="card-title mt-4">Criação: 09/01/2019 11:27</h6>
                                <h6 class="card-title">Conclusão: 10/01/2019 12:00</h6>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-cor-primaria">
                                <h5 class="modal-title" id="exampleModalLabel">Selecione quais certidões você deseja solicitar</h5>
                                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <form class="" action="" method="post">

                                <div class="modal-body">
                                    <div class="row justify-content-end mb-4">
                                        <div class="col-md-3">
                                            <i class="fas fa-square icon-legenda-accent-secundaria"></i> Não Selecionada
                                        </div>
                                        <div class="col-md-3">
                                            <i class="fas fa-square icon-legenda-accent-primaria"></i> Selecionada
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 mb-3 text-center">
                                            <div class="btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block">
                                                    <input type="checkbox" autocomplete="off">Federal
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3 text-center">
                                            <div class="btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block">
                                                    <input type="checkbox" autocomplete="off">Estadual
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3 text-center">
                                            <div class="btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block">
                                                    <input type="checkbox" autocomplete="off">Municipal
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 mb-3 text-center">
                                            <div class="btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block">
                                                    <input type="checkbox" autocomplete="off">Trabalhista
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3 text-center">
                                            <div class="btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block">
                                                    <input type="checkbox" autocomplete="off">Simplificada
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3 text-center">
                                            <div class="btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block">
                                                    <input type="checkbox" autocomplete="off">Falência
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 mb-3 text-center">
                                            <div class="btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block">
                                                    <input type="checkbox" autocomplete="off">Antecedentes Criminais
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3 text-center">
                                            <div class="btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block">
                                                    <input type="checkbox" autocomplete="off">Quitação Eleitoral
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3 text-center">
                                            <div class="btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block">
                                                    <input type="checkbox" autocomplete="off">FGTS
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary btn-padrao" data-dismiss="modal"><b>Ok</b></button>
                                    <button type="button" class="btn btn-secondary btn-padrao" data-dismiss="modal">Fechar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Right Panel -->

    <?php include __DIR__ . '/footer.php'; ?>

</body>
</html>
