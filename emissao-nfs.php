<!doctype html>
<html class="no-js" lang="pt-br">

<?php include __DIR__ . '/head.php'; ?>

<body>
    <!-- Left Panel -->

    <?php include __DIR__ . '/menu-left.php'; ?>

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <?php include __DIR__ . '/menu-right-header.php'; ?>

        <div id="carregando" class="center display-none">
            <div class="loading">
            </div>
        </div>

        <div class="container-fluid">
            <div class="content" id="conteudo">
                <div class="row mt-3 mb-4 justify-content-around">
                    <div class="col text-center">
                        <button type="button" class="btn mb-2 btn-padrao btn-lg btn-cor-primaria" data-toggle="modal" data-target="#modalNovaNfSimples">Emitir Nova Nota</button>
                        <!-- <button type="button" class="btn mb-2 btn-padrao btn-lg btn-cor-primaria" data-toggle="modal" data-target="#modalNovaNfSimples">Emitir Nova Nota Simples</button> -->
                        <!-- <button type="button" class="btn mb-2 btn-padrao btn-lg btn-cor-primaria" data-toggle="modal" data-target="#modalNovaNfPresumido">Emitir Nova Nota Presumido</button> -->
                    </div>
                </div>

                <?php if (array_key_exists('emissaoNota', $_COOKIE)) : ?>
                    <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Sucesso!</strong>
                        <?=$_COOKIE['resultadoEmissaoNota']?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php endif ?>

                <hr>
                <div class="row mt-4">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="fas fa-marker text-primary border-primary"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Total de Notas Fiscais Emitidas</div>
                                        <div class="stat-digit">100</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 text-center">
                        <h5 class="py-3 bg-cor-accent-primaria rounded-top">Últimas Nfs Emitidas</h5>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Tomador</th>
                                    <th scope="col">Serviço</th>
                                    <th scope="col">Data Emissão</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Mark</td>
                                    <td>Consulta</td>
                                    <td>01/01/2019</td>
                                </tr>
                                <tr>
                                    <td>Jacob</td>
                                    <td>Consulta</td>
                                    <td>01/01/2019</td>
                                </tr>
                                <tr>
                                    <td>Larry</td>
                                    <td>Consulta</td>
                                    <td>01/01/2019</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Right Panel -->

    <?php include __DIR__ . '/modal/nova-nf-simples.php'; ?>
    <?php include __DIR__ . '/modal/nova-nf-presumido.php'; ?>
    <?php include __DIR__ . '/modal/novo-servico.php'; ?>
    <?php include __DIR__ . '/footer.php'; ?>

</body>

</html>

<script>
    var cnae = [
        "8301",
        "8202",
        "8101",
    ];

    autocomplete(document.getElementById("cnae-nf"), cnae);
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#money').mask('000.000,00');
    });

    $(document).ready(function () {
        $('#money-im').mask('R$ 000,00');
    });
</script>

