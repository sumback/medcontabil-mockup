<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Medb">
    <meta name="author" content="Thiago Gabriel">

    <title>MedContábil | Área do Administrador</title>

    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-title" content="MedContábil"/>
    <meta name="msapplication-TileImage" content="img/favicon.png">

    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon.png">

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="img/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.png">
    <link rel="apple-touch-startup-image" href="img/favicon.png">

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="css/custom-css/login.css" type="text/css">
    <link rel="stylesheet" href="css/custom-css/loading.css" type="text/css">
</head>

<body class="text-center">
    <div id="carregando" class="center display-none">
        <div class="loading">
        </div>
    </div>

    <div class="form" id="conteudo">
        <form class="form-signin needs-validation-loading gif-loading-form" action="index.php" method="post" novalidate autocomplete="none">
            <img class="logo-login mt-3" src="img/medcontabil_admin.png">
            <div class="form-group text-left mt-5">
                <label for="usuario" class="label-login">Usuário</label>
                <input id="usuario" name="usuario" type="text" class="form-control" maxlength="35" required autofocus autocomplete="off">
                <div class="invalid-feedback">
                    Obrigatório.
                </div>
            </div>
            <div class="form-group text-left">
                <label for="senha" class="label-login">Senha</label>
                <input id="senha" name="senha" type="password" class="form-control" maxlength="35" required>
                <div class="invalid-feedback">
                    Obrigatório.
                </div>
            </div>
            <button class="btn btn-success btn-entrar text-uppercase mt-3" type="submit">Ok</button>
            <p class="mt-4 mb-3 marca-texto">&copy; MedContábil</p>
        </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js" charset="utf-8"></script>
    <script src="js/custom-js/loading-login.js" charset="utf-8"></script>


</body>
</html>
