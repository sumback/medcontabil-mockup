<!doctype html>
<html class="no-js" lang="pt-br">

    <?php include __DIR__ . '/head.php'; ?>

    <body>
        <!-- Left Panel -->

        <?php include __DIR__ . '/menu-left.php'; ?>

        <!-- Right Panel -->

        <div id="right-panel" class="right-panel">

            <?php include __DIR__ . '/menu-right-header.php'; ?>

            <div id="carregando" class="center display-none">
                <div class="loading">
                </div>
            </div>

            <div class="container-fluid">
                <div class="content mt-3" id="conteudo">
                    <div class="row justify-content-around">
                        <div class="col-md-9">
                            <div class="card">
                                <div class="card-header bg-cor-accent-primaria">
                                    <i class="fa fa-user"></i><strong class="card-title pl-md-2">Seu Perfil</strong>
                                </div>
                                <div class="card-body">
                                    <div class="mx-auto d-block">
                                        <img class="rounded-circle mx-auto d-block" src="img/favicon.png" alt="Card image cap">
                                        <h5 class="text-sm-center mt-2 mb-1">João Carlos</h5>
                                        <div class="location text-sm-center"><i class="fas fa-map-marker-alt"></i> Maringá, Paraná</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>RG</h6>
                                            <p class="text-dark mt-1">8888888888888</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6>CPF</h6>
                                            <p class="text-dark mt-1">888.888.888-88</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Telefone</h6>
                                            <p class="text-dark mt-1">(44) 9 9999-9999</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6>Email</h6>
                                            <p class="text-dark mt-1">exemplo@exemplo.com</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Endereço</h6>
                                            <p class="text-dark mt-1">Rua Aaaaaaaa, 148 - Maringá, Paraná</p>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <button class="btn btn-padrao btn-sm btn-cor-primaria" type="button" data-toggle="modal" data-target="#modalAlteraDados">Alterar Dados</button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Nome da Empresa</h6>
                                            <p class="text-dark mt-1">Exemplo</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6>CNPJ</h6>
                                            <p class="text-dark mt-1">1111111111111111</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Endereço</h6>
                                            <p class="text-dark mt-1">Rua Aaaaaaaa, 148 - Maringá, Paraná</p>
                                        </div>
                                    </div>
                                    <hr>

                                    <!-- ********** SOCIOS ********** -->

                                    <div class="row mb-3">
                                        <div class="col text-center">
                                            <h3>Socios</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Nome</h6>
                                            <p class="text-dark mt-1">Carlos Eduardo</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>RG</h6>
                                            <p class="text-dark mt-1">8888888888888</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6>CPF</h6>
                                            <p class="text-dark mt-1">888.888.888-88</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Telefone</h6>
                                            <p class="text-dark mt-1">(44) 9 9999-9999</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6>Email</h6>
                                            <p class="text-dark mt-1">exemplo@exemplo.com</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Porcentagem</h6>
                                            <p class="text-dark mt-1">10%</p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Nome</h6>
                                            <p class="text-dark mt-1">Luiz Carlos</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>RG</h6>
                                            <p class="text-dark mt-1">8888888888888</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6>CPF</h6>
                                            <p class="text-dark mt-1">888.888.888-88</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Telefone</h6>
                                            <p class="text-dark mt-1">(44) 9 9999-9999</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h6>Email</h6>
                                            <p class="text-dark mt-1">exemplo@exemplo.com</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Porcentagem</h6>
                                            <p class="text-dark mt-1">10%</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Right Panel -->

        <?php include __DIR__ . '/modal/alterar-dados-perfil.php'; ?>
        <?php include __DIR__ . '/footer.php'; ?>

    </body>
</html>
