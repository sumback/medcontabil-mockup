<!doctype html>
<html class="no-js" lang="pt-br">

<?php include __DIR__ . '/head.php'; ?>

<body>
   <!-- Left Panel -->

   <?php include __DIR__ . '/menu-left.php'; ?>

   <!-- Right Panel -->

   <div id="right-panel" class="right-panel">

      <?php include __DIR__ . '/menu-right-header.php'; ?>

      <div id="carregando" class="center display-none">
         <div class="loading">
         </div>
      </div>

      <div class="container-fluid">
         <div class="content" id="conteudo">
            <div class="row mt-3 mb-4 justify-content-around">
               <div class="col text-center">
                  <button type="button" class="btn mb-2 btn-padrao btn-lg btn-cor-primaria" data-toggle="modal" data-target="#modalDocContabilidade">Documentos Contabilidade</button>
                  <button type="button" class="btn mb-2 btn-padrao btn-lg btn-cor-primaria" data-toggle="modal" data-target="#modalDocRh">Documentos RH</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Right Panel -->

   <?php include __DIR__ . '/modal/envio-documentos/doc-contabilidade.php'; ?>
   <?php include __DIR__ . '/modal/envio-documentos/doc-rh.php'; ?>
   <?php include __DIR__ . '/modal/novo-servico.php'; ?>
   <?php include __DIR__ . '/footer.php'; ?>

</body>

</html>
