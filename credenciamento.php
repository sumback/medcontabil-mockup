<!doctype html>
<html class="no-js" lang="pt-br">

<?php include __DIR__ . '/head.php'; ?>

<body>
    <!-- Left Panel -->

    <?php include __DIR__ . '/menu-left.php'; ?>

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">
        
        <?php include __DIR__ . '/menu-right-header.php'; ?>

        <div id="carregando" class="center display-none">
            <div class="loading">
            </div>
        </div>

        <div class="container-fluid">
            <div class="content" id="conteudo">
                <div class="row mt-3">
                    <div class="col-md-9 mb-3">
                        <div class="row">
                            <div class="col text-dark-blue text-center text-md-left">
                                <h3>Credenciamento</h3>
                            </div>
                        </div>
                        <hr class="bg-dark">
                    </div>
                    <div class="col-md-3">
                        <div class="card bg-light mb-3 text-center rounded borda-cor-primaria">
                            <div class="card-header bg-cor-primaria p-2"><b>PREVISÃO</b></div>
                            <div class="card-body texto-padrao p-2">
                                <h3 class="card-title"><i class="fas fa-stopwatch"></i></h3>
                                <h4 class="card-text"><b>2 Dias</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col text-center">
                        <button type="button" class="btn btn-primary btn-padrao btn-cor-primaria" data-toggle="modal" data-target="#exampleModal">
                            <b>Nova Solicitação</b>
                        </button>
                    </div>
                </div>

                <!-- ********************* CARDS DE SOLICITAÇÕES ****************-->

                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="card texto-padrao bg-light mb-3 border-info rounded">
                            <div class="card-header bg-info text-white"><b>Solicitação Pendente</b></div>
                            <div class="card-body">
                                <h5 class="card-title">Tipo: Credenciamento</h5>
                                <h6 class="card-title mt-4">Data Criação: 09/01/2019 11:27</h6>
                                <h6 class="card-title">Previsão Conclusão: 10/01/2019 12:00</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="card texto-padrao bg-light mb-3 rounded border-success">
                            <div class="card-header bg-success text-white"><b>Solicitação Finalizada</b></div>
                            <div class="card-body">
                                <h5 class="card-title">Tipo: Credenciamento</h5>
                                <h6 class="card-title mt-4">Criação: 09/01/2019 11:27</h6>
                                <h6 class="card-title">Conclusão: 10/01/2019 12:00</h6>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header bg-cor-primaria">
                                <h5 class="modal-title" id="exampleModalLabel">Nova Solicitação de Credenciamento</h5>

                                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <form class="" action="" method="post">
                                <div class="modal-body">
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for=""><b>Enviar Edital</b></label>
                                            <div class="input-group mb-3">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03">
                                                    <label class="custom-file-label procurar-arquivo" for="inputGroupFile03">Escolha um arquivo</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for=""><b>Valor da Hora</b></label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">R$</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" id="money">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary btn-padrao" data-dismiss="modal"><b>Enviar</b></button>
                                    <button type="button" class="btn btn-secondary btn-padrao" data-dismiss="modal">Fechar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include __DIR__ . '/footer.php'; ?>

    <script type="text/javascript">

        $(document).ready(function(){
            $('#money').mask('000,00');
        });

    </script>

</body>
</html>
