<!doctype html>
<html class="no-js" lang="pt-br">

<?php include __DIR__ . '/head.php'; ?>

<body>
    <!-- Left Panel -->

    <?php include __DIR__ . '/menu-left.php'; ?>

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <?php include __DIR__ . '/menu-right-header.php'; ?>  

        <div id="carregando" class="center display-none">
            <div class="loading">
            </div>
        </div>

        <div class="container-fluid">
            <div class="content mt-3" id="conteudo">
                <div class="row">
                    <div class="col text-dark-blue text-center text-md-left mb-3">
                        <h3>Minhas Ordens de Serviço</h3>
                        <hr class="bg-dark">
                    </div>
                </div>
                <div class="row justify-content-around mb-5">
                    <div class="col-md-3 mt-3 text-center">
                        <label class="text-dark-blue"><b>Tipo</b></label>
                        <select class="form-control" name="">
                            <option value="todos">Todos</option>
                            <option value="credenciamento">Credenciamento</option>
                            <option value="faturamento">Faturamento</option>
                            <option value="emissao-certidao">Emissão de Certidão</option>
                            <option value="recalculo-guia">Recálculo de Guias</option>
                            <option value="alteracao-contratual">Alteração Contratual</option>
                            <option value="outros">Outros</option>
                        </select>
                    </div>
                    <div class="col-md-3 mt-3 text-center">
                        <label class="text-dark-blue"><b>Status</b></label>
                        <select class="form-control" name="">
                            <option value="pendente">Pendente</option>
                            <option value="finalizados">Finalizada</option>
                        </select>
                    </div>
                    <div class="col-md-3 mt-3 text-center">
                        <label class="text-dark-blue"><b>Período</b></label>
                        <select class="form-control" name="">
                            <option value="30-dias">Últimos 30 dias</option>
                            <option value="60-dias">Últimos 60 dias</option>
                        </select>
                    </div>
                </div>

                <!-- ********************* CARDS DE SOLICITAÇÕES ****************-->

                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="card texto-padrao bg-light mb-3 border-info rounded">
                            <div class="card-header bg-info text-white"><b>Solicitação Pendente</b></div>
                            <div class="card-body">
                                <h5 class="card-title">Tipo: Credenciamento</h5>
                                <h6 class="card-title mt-4">Data Criação: 09/01/2019 11:27</h6>
                                <h6 class="card-title">Previsão Conclusão: 10/01/2019 12:00</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="card texto-padrao bg-light mb-3 border-info rounded">
                            <div class="card-header bg-info text-white"><b>Solicitação Pendente</b></div>
                            <div class="card-body">
                                <h5 class="card-title">Tipo: Faturamento</h5>
                                <h6 class="card-title mt-4">Data Criação: 09/01/2019 11:27</h6>
                                <h6 class="card-title">Previsão Conclusão: 10/01/2019 12:00</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="card texto-padrao bg-light mb-3 border-info rounded">
                            <div class="card-header bg-info text-white"><b>Solicitação Pendente</b></div>
                            <div class="card-body">
                                <h5 class="card-title">Tipo: Emissão de Certidões</h5>
                                <h6 class="card-title mt-4">Data Criação: 09/01/2019 11:27</h6>
                                <h6 class="card-title">Previsão Conclusão: 10/01/2019 12:00</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 mb-5">
                    <div class="col-md-4 text-center">
                        <div class="card texto-padrao bg-light mb-3 rounded border-success">
                            <div class="card-header bg-success text-white"><b>Solicitação Finalizada</b></div>
                            <div class="card-body">
                                <h5 class="card-title">Tipo: Recálculo de Guias</h5>
                                <h6 class="card-title mt-4">Criação: 09/01/2019 11:27</h6>
                                <h6 class="card-title">Conclusão: 10/01/2019 12:00</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="card texto-padrao bg-light mb-3 rounded border-success">
                            <div class="card-header bg-success text-white"><b>Solicitação Finalizada</b></div>
                            <div class="card-body">
                                <h5 class="card-title">Tipo: Alteração Contratual</h5>
                                <h6 class="card-title mt-4">Criação: 09/01/2019 11:27</h6>
                                <h6 class="card-title">Conclusão: 10/01/2019 12:00</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="card texto-padrao bg-light mb-3 rounded border-success">
                            <div class="card-header bg-success text-white"><b>Solicitação Finalizada</b></div>
                            <div class="card-body">
                                <h5 class="card-title">Tipo: Outros</h5>
                                <h6 class="card-title mt-4">Criação: 09/01/2019 11:27</h6>
                                <h6 class="card-title">Conclusão: 10/01/2019 12:00</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-dark-blue">
                        <h5 class="modal-title" id="exampleModalLabel">Nova Solicitação</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-3">
                            <div class="col">
                                <label for=""><b>Enviar Arquivo</b></label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-outline-secondary btn-padrao" type="button" id="inputGroupFileAddon03">Enviar</button>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03">
                                        <label class="custom-file-label procurar-arquivo" for="inputGroupFile03">Escolha um arquivo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for=""><b>Data de Vencimento do Arquivo</b></label>
                                <input class="form-control" type="text" name="" value="">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-padrao"><b>Salvar</b></button>
                        <button type="button" class="btn btn-secondary btn-padrao" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header btn-medium-blue">
                        <h5 class="modal-title" id="exampleModalLabel2">Excluir Solicitação</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="text-dark-blue">Tem certeza que deseja excluir a solicitação?</h5>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-padrao"><b>Sim</b></button>
                        <button type="button" class="btn btn-secondary btn-padrao" data-dismiss="modal">Não</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Right Panel -->

    <?php include __DIR__ . '/footer.php'; ?>

</body>
</html>
