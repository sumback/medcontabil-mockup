<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MedContábil | Cliente</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="/img/favicon.png"> -->

    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-title" content="MedContábil"/>

    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon.png">

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="img/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon.png">

    <link rel="apple-touch-startup-image" href="img/favicon.png">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom-css/index.css">
    <link rel="stylesheet" href="css/custom-css/loading.css">
    <!-- <link rel="stylesheet" href="css/custom-css/linha-do-tempo.css"> -->
    <link rel="stylesheet" href="css/custom-css/os.css">
    <link rel="stylesheet" href="css/custom-css/autocomplete.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>
