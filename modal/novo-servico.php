<div class="modal fade " id="modalNovoServico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header bg-cor-primaria">
                <h5 class="modal-title" id="exampleModalLabel">Cadastrar Novo Serviço</h5>
            </div>
            <form class="" action="" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label><strong>Código do Serviço</strong></label>
                            <input class="form-control" type="text">
                        </div>
                        <div class="col-md-6">
                            <label><strong>CNAE</strong></label>
                            <input class="form-control" type="text">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <label><strong>Aliquota do ISS</strong></label>
                            <input class="form-control" type="text">
                        </div>
                        <div class="col-md-6">
                            <label><strong>Valor do Serviço</strong></label>
                            <input class="form-control" type="text">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-padrao btn-primary">Emitir</button>
                    <button type="button" class="btn btn-padrao btn-secondary" data-dismiss="modal">Fecha</button>
                </div>
            </form>
        </div>
    </div>
</div>
