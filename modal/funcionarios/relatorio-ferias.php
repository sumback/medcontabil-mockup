<div class="modal fade bd-example-modal-lg" id="modalRelatorioFerias" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-cor-primaria">
                <h5 class="modal-title" id="exampleModalLabel">Obter Relatório de Férias</h5>
            </div>
            <form action="" method="post" autocomplete="off">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 mb-3 autocomplete">
                            <label for=""><strong>Nome do Funcionário</strong></label>
                            <input class="form-control" type="text" id="input-consulta-ferias-nome">
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-7">
                            <label for=""><strong>Perído</strong></label>
                            <div class="input-group">
                                <input type="date" class="form-control">
                                <input type="date" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-padrao" data-dismiss="modal">Solicitar</button>
                    <button type="button" class="btn btn-secondary btn-padrao" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>
