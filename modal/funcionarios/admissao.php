<div class="modal fade bd-example-modal-lg" id="modalAdmissao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-cor-primaria">
                <h5 class="modal-title" id="exampleModalLabel">Nova Solicitação de Admissão</h5>
            </div>
            <form action="index.html" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7 mb-3">
                            <label for=""><strong>Nome do Funcionário</strong></label>
                            <input class="form-control" type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label for=""><strong>Telefone do Funcionário</strong></label>
                            <input class="form-control" type="text">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for=""><strong>Email do Funcionário</strong></label>
                            <input class="form-control" type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label for=""><strong>Cargo/Função</strong></label>
                            <input class="form-control" type="text">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for=""><strong>Salário</strong></label>
                            <input class="form-control" type="text">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for=""><strong>Data de Início</strong></label>
                            <input class="form-control" type="date">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-padrao" data-dismiss="modal"><b>Ok</b></button>
                    <button type="button" class="btn btn-secondary btn-padrao" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>
