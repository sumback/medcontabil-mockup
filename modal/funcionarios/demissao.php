<div class="modal fade bd-example-modal-lg" id="modalDemissao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-cor-primaria">
                <h5 class="modal-title" id="exampleModalLabel">Nova Solicitação de Demissão</h5>
            </div>
            <form action="index.html" method="post" autocomplete="off">
                <div class="modal-body">                    
                    <div class="row">
                        <div class="col-md-4 mb-3 autocomplete">
                            <label><strong>Nome do Funcionário</strong></label>
                            <input class="form-control" type="text" id="input-demissao-nome">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for=""><strong>Último dia trabalhado</strong></label>
                            <input class="form-control" type="date">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 mb-3">
                            <label for=""><strong>Tipo de Demissão</strong></label>
                            <div class="btn-group-toggle mb-3" data-toggle="buttons">
                                <label class="btn btn-cor-accent-secundaria btn-padrao mb-2">
                                    <input type="radio" name="options" id="option1" autocomplete="off" checked>Dentro prazo de experiência
                                </label>
                                <label class="btn btn-cor-accent-secundaria btn-padrao mb-2">
                                    <input type="radio" name="options" id="option2" autocomplete="off">Termino do contrato
                                </label>
                            </div>                            
                            <div class="input-group mb-3">
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected>Escolher...</option>
                                    <option value="1">Acordo Entre as Partes</option>
                                    <option value="2">Pedido de Demissão</option>
                                    <option value="3">Iniciativa Empregador</option>
                                    <option value="3">Justa Causa</option>
                                </select>
                            </div>                            
                        </div>                      
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for=""><strong>Exame Demissional</strong></label>
                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                    <label class="custom-file-label procurar-arquivo" for="inputGroupFile01"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-padrao" data-dismiss="modal"><b>Ok</b></button>
                    <button type="button" class="btn btn-secondary btn-padrao" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>