<div class="modal fade bd-example-modal-lg" id="modalFerias" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-cor-primaria">
                <h5 class="modal-title" id="exampleModalLabel">Férias</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 mb-3 text-center">
                        <button type="button" class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block" data-toggle="modal" data-dismiss="modal" data-target="#modalRelatorioFerias">Obter Relatório</button>
                    </div>
                    <div class="col-md-6 mb-3 text-center">
                        <button type="button" class="btn btn-padrao btn-lg btn-cor-accent-secundaria btn-block" data-toggle="modal" data-dismiss="modal" data-target="#modalNovaFerias">Emitir Novas Férias</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-padrao" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
