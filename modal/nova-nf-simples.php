<div class="modal fade bd-example-modal-lg" id="modalNovaNfSimples" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-cor-primaria">
                <h5 class="modal-title" id="exampleModalLabel">Emitir Nova Nota Fiscal Simples</h5>
            </div>
            <form class="needs-validation" action="../emite-nota.php" method="post" autocomplete="off" novalidate>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5 autocomplete">
                            <label><strong>Código de Serviço</strong></label>
                            <!-- <input class="form-control" type="text" id="cnae-nf"> -->
                            <select class="form-control" name="" id="" required>
                                <option value="">Escolha...</option>
                                <option value="">4.01 - Medicina e biomedicina</option>
                                <option value="">4.03 - Hospitáis, clínicas, laboratórios, etc</option>
                            </select>
                            <div class="invalid-feedback">
                                Campo Obrigatório.
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label><strong>CNPJ do Tomador</strong></label>
                            <input class="form-control" type="text" required>
                            <div class="invalid-feedback">
                                Campo Obrigatório.
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label><strong>Valor do Serviço</strong></label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">R$</span>
                                </div>
                                <input type="text" class="form-control" id="money" required>
                                <div class="invalid-feedback">
                                    Campo Obrigatório.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-8">
                            <label><strong>Descrição</strong></label>
                            <textarea rows="4" cols="50" class="form-control" type="text" required>Honorários médicos&#013; &#013;Conta: 7898 | Agência: 789456-23
                            </textarea>
                            <div class="invalid-feedback">
                                Campo Obrigatório.
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-5">
                            <label for=""><strong>Email para envio da NFS-e</strong></label>
                            <input class="form-control" required>
                            <div class="invalid-feedback">
                                Campo Obrigatório.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-padrao btn-primary">Emitir</button>
                    <button type="button" class="btn btn-padrao btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>