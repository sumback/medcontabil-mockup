<div class="modal fade" id="modalDocRh" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
   aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header bg-cor-primaria">
            <h5 class="modal-title" id="exampleModalLabel">Enviar Documentos RH</h5>
         </div>
         <form class="needs-validation" action="" method="post" enctype="multipart/form-data" autocomplete="off" novalidate>
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-8 autocomplete">
                  <input type="file" name="file" id="file" class="inputfile" data-multiple-caption="{count} arquivos selecionados" multiple />
                     <label for="file" class="btn btn-block btn-padrao btn-cor-accent-primaria"><span>Selecionar os Arquivos</span></label>                     
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-padrao btn-primary">Enviar</button>
               <button type="button" class="btn btn-padrao btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
         </form>
      </div>
   </div>
</div>