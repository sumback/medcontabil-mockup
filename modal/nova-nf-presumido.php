<div class="modal fade bd-example-modal-lg" id="modalNovaNfPresumido" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-cor-primaria">
                <h5 class="modal-title" id="exampleModalLabel">Emitir Nova Nota Fiscal Presumido</h5>
            </div>
            <form action="" method="post" autocomplete="off">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 autocomplete">
                            <label><strong>Código de Serviço</strong></label>
                            <input class="form-control" type="text" id="cnae-nf">
                        </div>
                        <div class="col-md-4">
                            <label><strong>CNPJ do Tomador</strong></label>
                            <input class="form-control" type="text">
                        </div>
                        <div class="col-md-3">
                            <label><strong>Valor do Serviço</strong></label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">R$</span>
                                </div>
                                <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" id="money">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">   
                        <div class="col-md-9">
                            <label><strong>Descrição</strong></label>
                            <textarea rows="4" cols="50" class="form-control" type="text">Conta: 7898 | Agência: 789456-23</textarea>
                        </div>
                                          
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-5">
                            <label for=""><strong>Impostos</strong></label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-cor-accent-secundaria rounded-left">
                                            <input type="checkbox" checked autocomplete="off"> ISS
                                        </label>
                                    </div>
                                </div>
                                <input type="text" class="form-control" value="R$ 100,00">
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-cor-accent-secundaria rounded-left">
                                            <input type="checkbox" checked autocomplete="off">IRPS - 1.5%
                                        </label>
                                    </div>
                                </div>
                                <input type="text" class="form-control" value="R$ 100,00">
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-cor-accent-secundaria rounded-left">
                                            <input type="checkbox" checked autocomplete="off">PIS - 0.65%
                                        </label>
                                    </div>
                                </div>
                                <input type="text" class="form-control" placeholder="R$ 90,00" readonly>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-cor-accent-secundaria rounded-left">
                                            <input type="checkbox" checked autocomplete="off">COFINS - 3%
                                        </label>
                                    </div>
                                </div>
                                <input type="text" class="form-control" placeholder="R$ 200,00" readonly>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-cor-accent-secundaria rounded-left">
                                            <input type="checkbox" checked autocomplete="off">CSLL - 1%
                                        </label>
                                    </div>
                                </div>
                                <input type="text" class="form-control" placeholder="R$ 100,00" readonly>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <label for=""><strong>Email para envio da NFS-e</strong></label>
                            <input class="form-control">
                        </div>                         
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-padrao btn-primary" data-dismiss="modal">Emitir</button>
                    <button type="button" class="btn btn-padrao btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>
