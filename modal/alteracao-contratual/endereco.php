<div class="modal fade" id="modalEndereco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-cor-primaria">
                <h5 class="modal-title" id="exampleModalLabel">Alterar Endereço</h5>
            </div>
            <form class="" action="" method="post">

                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <label for=""><strong>Enviar comprovante do novo endereço</strong></label>
                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01">
                                    <label class="custom-file-label procurar-arquivo" for="inputGroupFile01">Selecionar arquivo</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-padrao" data-dismiss="modal">Enviar</button>
                    <button type="button" class="btn btn-secondary btn-padrao" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>
